Note: This is the final project. I spent most of my time on the data and experiment projects.

<br/>

- Dependencies:
    - commons-io: downloading and managing files.
    - com.googlecode.json-simple: parsing json object
    - org.apache.commons: creating .CSV file

<br/>

- How to run:
    - gradle build
    - gradle run --args="[datetime_start] [datetime_end]"
        - Example: gradle run --args="2019-08-01T00:00:00Z 2019-08-01T01:00:00Z"
    - Download and result location: project-dir/lib
    - The downloaded file will be deleted after finishing.
        
<br/>

- Technical decisions:
    - All the classes in functional programming is  must be immutable, which means their state cannot be changed. Since Java does not support named parameter, positional parameter, or factory constructor to update the object so I decided to go with the builder design pattern. It is simply used to shorten the constructor. However, if it needs more attributes, adding more would be a hassle. I am working on a better solution.
    - Health metric:
        - All the time is measured in second.
        - Instead of divide all the scores by the max value, I normalize them using the formula: (score - min) / (max - min). The result is still between 0 and 1, where 0 represents the least of possible health and 1 represents the most of it.
        - "Average time for a pull request to get merged": For normal circumstances, the lesser the time, the greater the score is. So divide it by the minimum time will not represent maximum health. I also found out that some issues are opened and then closed immediately, which will make the minimum time equal to 0. So I try a simpler approach which is to normalize and return 1 minus the score. However, if the repository does not have any pull request, it will have a full score. I wonder if this is considered "healthy"? Similar to the "Average time for a pull request to get merged" metric.
