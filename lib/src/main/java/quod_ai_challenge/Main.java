package quod_ai_challenge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main {
	
	public static long dayDiff = 1;
	
	static Hashtable<String, Repository> repoDict = new Hashtable<String, Repository>();
	
	// store max and min value
	public static MaxMinStat<Long> commitNumberStat = new MaxMinStat<Long>(0l, Long.MAX_VALUE);
	public static MaxMinStat<Long> openPullRequestCountStat = new MaxMinStat<Long>(0l, Long.MAX_VALUE);
	public static MaxMinStat<Long> releaseCountStat = new MaxMinStat<Long>(0l, Long.MAX_VALUE);
	public static MaxMinStat<Long> issueOpenerNumberStat = new MaxMinStat<Long>(0l, Long.MAX_VALUE);
	public static MaxMinStat<Double> commitPerDevStat = new MaxMinStat<Double>(0.0, Double.MAX_VALUE);
	public static MaxMinStat<Double> avgCommentPerPullRequestStat = new MaxMinStat<Double>(0.0, Double.MAX_VALUE);
	public static MaxMinStat<Double> avgIssueOpenTimeStat = new MaxMinStat<Double>(0.0, Double.MAX_VALUE);
	public static MaxMinStat<Double> avgMergedTimeStat = new MaxMinStat<Double>(0.0, Double.MAX_VALUE);
	
	static void loadRepoBetween(ZonedDateTime startDate, ZonedDateTime endDate) {
		ZonedDateTime tempTime = startDate;
		JSONParser parser = new JSONParser();
	    
		while(tempTime.isBefore(endDate)) {
			String fileName = FileController.getFileName(tempTime);
			FileController.downloadFile(fileName);
			
			System.out.println("Processing " + fileName);
			
			File jsonFile = new File(fileName);
			
			try {
				BufferedReader br = new BufferedReader(new FileReader(jsonFile));
				String line;
		        while ((line = br.readLine()) != null) {
		        	JSONObject json = (JSONObject) parser.parse(line);
		        	processJson(json);
		        }
		        br.close();
		        Files.deleteIfExists(jsonFile.toPath());
		        tempTime = tempTime.plusHours(1);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
	
	static void processJson(JSONObject json) {
		JSONObject repoObj = (JSONObject) json.get("repo");
		String repoName = (String) repoObj.get("name");
		String eventType = (String) json.get("type");
		
		if (!repoDict.containsKey(repoName)) {
			String[] parts = repoName.split("/");
			long repoId = (long) repoObj.get("id");
			Repository repo = new Repository(repoId, parts[0], parts[1]);
			repoDict.put(repoName, repo);
		}
		
		switch (eventType) {
			case "PushEvent":
				updateCommitCount(json, repoName);
				updateDevCount(json, repoName);
				break;
			case "IssuesEvent":
				handleIssueEvent(json, repoName);
				break;
			case "PullRequestEvent":
				handlePullRequestEvent(json, repoName);
				break;
			case "ReleaseEvent":
				handleReleaseEvent(repoName);
				break;
			case "PullRequestReviewCommentEvent":
				handlePullRequestReviewCommentEvent(repoName);
				break;
			default:
				break;
		}
	}
	
	static void updateCommitCount(JSONObject json, String repoName) {
		Repository repo = repoDict.get(repoName);
		JSONObject payload = (JSONObject) json.get("payload");
		long commitNum = (long) payload.get("size");
		repo = repo.addCommit(commitNum);
		repoDict.put(repoName, repo);
	}
	
	static void updateDevCount(JSONObject json, String repoName) {
		Repository repo = repoDict.get(repoName);
		JSONObject payload = (JSONObject) json.get("payload");
		JSONArray commits = (JSONArray) payload.get("commits");
		for (Object commitObj : commits) {
			JSONObject commit = (JSONObject) commitObj;
			JSONObject author = (JSONObject) commit.get("author");
			String authorName = (String) author.get("name");
			repo = repo.addDeveloper(authorName);
		}
		repoDict.put(repoName, repo);
	}
	
	static void handleIssueEvent(JSONObject json, String repoName) {
		Repository repo = repoDict.get(repoName);
		JSONObject payload = (JSONObject) json.get("payload");
		String action = (String) payload.get("action");
		JSONObject issue = (JSONObject) payload.get("issue");
		JSONObject user = (JSONObject) issue.get("user");
		String userName = (String) user.get("login");
		repo = repo.addIssueOpener(userName);
		
		if (action.equals("opened")) {
			repo = repo.addOpenedIssue();
		} else if (action.equals("closed")) {
			String createdAt = (String) issue.get("created_at");
			String closedAt = (String) issue.get("closed_at");
			if (createdAt != null && closedAt != null) {
				ZonedDateTime createdDate =  Utils.convertToDateTime(createdAt);
				ZonedDateTime closedDate =  Utils.convertToDateTime(closedAt);
				repo = repo.addClosedIssue(createdDate, closedDate);
			}
		}
		
		repoDict.put(repoName, repo);
	}
	
	static void handlePullRequestEvent(JSONObject json, String repoName) {
		Repository repo = repoDict.get(repoName);
		JSONObject payload = (JSONObject) json.get("payload");
		String action = (String) payload.get("action");
		JSONObject pullRequest = (JSONObject) payload.get("pull_request");
		String createdAt = (String) pullRequest.get("created_at");
		String mergedAt = (String) pullRequest.get("merged_at");
		long id = (long) pullRequest.get("id");
		
		if (action.equals("opened")) {
			repo = repo.addPullRequest(id);
		} else if (action.equals("closed") && createdAt != null && mergedAt != null) {
			ZonedDateTime createdDate =  Utils.convertToDateTime(createdAt);
			ZonedDateTime mergedDate =  Utils.convertToDateTime(mergedAt);
			repo = repo.addMergeEvent(id, createdDate, mergedDate);
		}
		
		repoDict.put(repoName, repo);
	}
	
	static void handleReleaseEvent(String repoName) {
		Repository repo = repoDict.get(repoName);
		repo = repo.addRelease();
		repoDict.put(repoName, repo);
	}
	
	static void handlePullRequestReviewCommentEvent(String repoName) {
		Repository repo = repoDict.get(repoName);
		repo = repo.addPullRequestComment();
		repoDict.put(repoName, repo);
	}
	
	static void updateMaxMin(Repository repo) {
		commitNumberStat = commitNumberStat.updateValue(repo.commitNumber);
		commitPerDevStat = commitPerDevStat.updateValue(repo.getCommitPerDeveloper());
		avgIssueOpenTimeStat = avgIssueOpenTimeStat.updateValue(repo.getAvgIssueOpenTime());
		openPullRequestCountStat = openPullRequestCountStat.updateValue(repo.openPullRequestCount);
		avgMergedTimeStat = avgMergedTimeStat.updateValue(repo.getAvgMergeTime());
		releaseCountStat = releaseCountStat.updateValue(repo.releaseNumber);
		issueOpenerNumberStat = issueOpenerNumberStat.updateValue(repo.issueOpenerCount);
		avgCommentPerPullRequestStat = avgCommentPerPullRequestStat.updateValue(repo.getAvgCommentPerPullRequest());
	}
	
	static double calculateHealthScore(Repository repo) {
		double commitNumberScore = commitNumberStat.normalize(repo.commitNumber);
		double openPullRequestCountScore = openPullRequestCountStat.normalize(repo.openPullRequestCount);
		double releaseCountScore = releaseCountStat.normalize(repo.releaseNumber);
		double commitPerDevScore = commitPerDevStat.normalize(repo.getCommitPerDeveloper());
		double issueOpenerNumberScore = issueOpenerNumberStat.normalize(repo.issueOpenerCount);
		double avgCommentPerPullRequestScore = avgCommentPerPullRequestStat.normalize(repo.getAvgCommentPerPullRequest());
		
		//Lesser value -> Greater Score
		double avgIssueOpenTimeScore = 1 - avgIssueOpenTimeStat.normalize(repo.getAvgIssueOpenTime());
		double avgMergedTimeScore = 1 - avgMergedTimeStat.normalize(repo.getAvgMergeTime());
		
		return commitNumberScore
				+ openPullRequestCountScore
				+ releaseCountScore
				+ commitPerDevScore
				+ avgIssueOpenTimeScore
				+ avgMergedTimeScore
				+ issueOpenerNumberScore
				+ avgCommentPerPullRequestScore;
	}
	
	static ArrayList<Map.Entry<String, Repository>> sortByHealthScore(ArrayList<Map.Entry<String, Repository>> arrList) {
		System.out.println("Sorting");
	    Collections.sort(arrList, new Comparator<Map.Entry<String, Repository>>(){
			@Override
			public int compare(Entry<String, Repository> o1, Entry<String, Repository> o2) {
				double value1 = calculateHealthScore(o1.getValue());
				double value2 = calculateHealthScore(o2.getValue());
				if (value1 > value2) {
					return -1;
				}
				else if (value1 < value2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
		return arrList;
	}
	
	static String[] getHeaders() {
		return new String[] {
				"org",
				"repo_name",
				"health_score",
				"num_commits",
				"num_contributors",
				"avg_num_of_commits_per_day",
				"avg_issues_open_time",
				"avg_merged_time",
				"commits_per_dev",
				"avg_comment_per_pull_request",
				"release_number",
				"issue_opener_number",
				"open_pull_request_number"	
		};
	}
	
	
	
	static void createCSV(String fileName, List<Entry<String, Repository>> repoList) {
		System.out.println("Creating " + fileName);
		Writer writer;
		try {
			writer = Files.newBufferedWriter(Paths.get(fileName));
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(getHeaders()));
			
			DecimalFormat df2 = new DecimalFormat("#.####");
		    
		    for (Entry<String, Repository> repoEntry : repoList) {
		    	Repository repo = repoEntry.getValue();
		        csvPrinter.printRecord(
		        		repo.orgName,
		        		repo.repoName,
		        		df2.format(calculateHealthScore(repo)),
		        		Long.toString(repo.commitNumber),
		        		Long.toString(repo.developerCount),
		        		df2.format(((double) repo.commitNumber) / dayDiff),
		        		df2.format(repo.getAvgIssueOpenTime()),
		        		df2.format(repo.getAvgMergeTime()),
		        		df2.format(repo.getCommitPerDeveloper()),
		        		df2.format(repo.getAvgCommentPerPullRequest()),
		        		Long.toString(repo.releaseNumber),
		        		Long.toString(repo.issueOpenerCount),
		        		Long.toString(repo.openPullRequestCount)
		        );
		    }
		    
		    csvPrinter.flush();
		    csvPrinter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		String startString = args[0];
		String endString = args[1];
		
		ZonedDateTime startDate =  Utils.convertToDateTime(startString);
		ZonedDateTime endDate =  Utils.convertToDateTime(endString);
		
		if (startDate.isBefore(endDate)) {
			long duration = Duration.between( startDate , endDate ).toDays();
			dayDiff = (duration == 0) ? 1 : duration;
		} else {
			System.out.println("Invalid Date");
			return;
		}
		
		loadRepoBetween(startDate, endDate);
		
		ArrayList<Map.Entry<String, Repository>> repoArrList = new ArrayList<Map.Entry<String, Repository>>(repoDict.entrySet());
		
		for (Entry<String, Repository> repoEntry : repoArrList) {
			updateMaxMin(repoEntry.getValue());
		}
		
		ArrayList<Map.Entry<String, Repository>> sortedRepoArrList = sortByHealthScore(repoArrList);
		
		createCSV("health_scores.csv", sortedRepoArrList.subList(0, 1000));
		
		System.out.println("Done");
	  }
}
