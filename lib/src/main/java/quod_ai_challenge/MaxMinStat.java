package quod_ai_challenge;

public class MaxMinStat<T> {
	
	final T max;
	final T min;
	
	MaxMinStat(T max, T min) {
		this.max = max;
		this.min = min;
	}
	
	MaxMinStat<T> updateValue(T value) {
		T newMax = compare(value, max) == 1 ? value : max;
		T newMin = compare(value, min) == -1 ? value : min;
		return new MaxMinStat<T>(newMax, newMin);
	}
	
	double normalize(T value) {
		if (value instanceof Double) {
			return (double) ((double) value - (double) min) / ((double) max - (double) min);
		} else if (value instanceof Long) {
			return (double) ((long) value - (long) min) / ((long) max - (long) min);
		}
		return 0; 
	}
	
	int compare(T v1, T v2) {
		if (v1 instanceof Double && v2 instanceof Double) {
			double o1 = (double) v1;
			double o2 = (double) v2;
			if (o1 > o2) {
				return 1;
			} else if (o1 < o2) {
				return -1;
			} else {
				return 0;
			}
		} else if (v1 instanceof Long && v2 instanceof Long) {
			long o1 = (long) v1;
			long o2 = (long) v2;
			if (o1 > o2) {
				return 1;
			} else if (o1 < o2) {
				return -1;
			} else {
				return 0;
			}
		}
		return 0;
	}
}
