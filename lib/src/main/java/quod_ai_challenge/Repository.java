package quod_ai_challenge;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class Repository {
	public Repository(long id, String orgName, String repoName) {
		this.id = id;
		this.orgName = orgName;
		this.repoName = repoName;
		
		this.commitNumber = 0;
		
		this.developerSet = new HashSet<String>();
		this.developerCount = 0;
		
		this.issueOpenerSet = new HashSet<String>();
		this.issueOpenerCount = 0;
		this.totalIssueOpenTime = 0;
		this.openedIssueNumber = 0;
		this.closedIssueNumber = 0;
		
		this.pullRequestSet = new HashSet<Long>();
		this.commentNumber = 0;
		this.openPullRequestCount = 0;
		this.totalMergeTime = 0;
		this.mergeNumber = 0;
		
		this.releaseNumber = 0;
	}
	
	Repository(
			long id,
			String orgName,
			String repoName,
			long commitNumber,
			Set<String> developerSet,
			long developerCount,
			Set<String> IssueOpenerSet,
			long IssueOpenerCount,
			long totalIssueOpenTime,
			long openedIssueNumber,
			long closedIssueNumber,
			Set<Long> pullRequestSet,
			long commentNumber,
			long openPullRequestCount,
			long totalMergeTime,
			long mergeNumber,
			long releaseNumber
			) {
		this.id = id;
		this.orgName = orgName;
		this.repoName = repoName;
		
		this.commitNumber = commitNumber;
		
		this.developerSet = developerSet;
		this.developerCount = developerCount;
		
		this.issueOpenerSet = IssueOpenerSet;
		this.issueOpenerCount = IssueOpenerCount;
		this.totalIssueOpenTime = totalIssueOpenTime;
		this.openedIssueNumber = openedIssueNumber;
		this.closedIssueNumber = closedIssueNumber;
		
		this.pullRequestSet = pullRequestSet;
		this.commentNumber = commentNumber;
		this.openPullRequestCount = openPullRequestCount;
		this.totalMergeTime = totalMergeTime;
		this.mergeNumber = mergeNumber;
		
		this.releaseNumber = releaseNumber;
	}
	
	final long id;
	final String repoName;
	final String orgName;
	
	final long commitNumber;
	
	final Set<String> developerSet;
	final long developerCount;
	
	final Set<String> issueOpenerSet;
	final long issueOpenerCount;
	final long totalIssueOpenTime;
	final long openedIssueNumber;
	final long closedIssueNumber;
	
	final Set<Long> pullRequestSet;
	final long openPullRequestCount;
	final long commentNumber;
	final long totalMergeTime;
	final long mergeNumber;
	
	final long releaseNumber;
	
	Repository addCommit(long value) {
		long newCommitCount = this.commitNumber + 1;
		return new RepositoryBuilder(this)
				.withCommitNumber(newCommitCount)
				.build();
	}

	Repository addDeveloper(String name) {
		if (!developerSet.contains(name)) {
			Set<String> newDeveloperSet = this.developerSet;
			newDeveloperSet.add(name);
			long newDeveloperCount = this.developerCount + 1;
			return new RepositoryBuilder(this)
					.withDeveloperSet(newDeveloperSet)
					.withDeveloperCount(newDeveloperCount)
					.build();
		}
		return this;
	}
	
	double getCommitPerDeveloper() {
		return (developerCount > 0) ? (((double) commitNumber) / developerCount) : 0.0;
	}
	
	Repository addIssueOpener(String name) {
		if (!issueOpenerSet.contains(name)) {
			Set<String> newIssueOpenerSet = this.issueOpenerSet;
			newIssueOpenerSet.add(name);
			long newIssueOpenerCount = this.issueOpenerCount + 1;
			return new RepositoryBuilder(this)
					.withIssueOpenerSet(newIssueOpenerSet)
					.withIssueOpenerCount(newIssueOpenerCount)
					.build();
		}
		return this;
	}
	
	double getClosedOpenedIssueRatio() {
		if (openedIssueNumber == 0) {
			return 0.0;
		}
		return ((double) closedIssueNumber) / openedIssueNumber;
	}
	
	Repository addOpenedIssue() {
		long newOpenedIssueNumber = this.openedIssueNumber + 1;
		return new RepositoryBuilder(this)
				.withOpenedIssueNumber(newOpenedIssueNumber)
				.build();
	}
	
	Repository addClosedIssue(ZonedDateTime openedDate, ZonedDateTime closedDate) {
		long openDuration = Duration.between( openedDate , closedDate ).toSeconds();
		long newTotalIssueOpenTime = this.totalIssueOpenTime + openDuration;
		long newClosedIssueNumber = this.closedIssueNumber + 1;
		return new RepositoryBuilder(this)
				.withTotalIssueOpenTime(newTotalIssueOpenTime)
				.withClosedIssueNumber(newClosedIssueNumber)
				.build();
	}
	
	double getAvgIssueOpenTime() {
		if (closedIssueNumber == 0) {
			return 0.0;
		}
		return ((double) totalIssueOpenTime) / closedIssueNumber;
	}
	
	Repository addPullRequest(long id) {
		if (!pullRequestSet.contains(id)) {
			Set<Long> newPullRequestSet = this.pullRequestSet;
			newPullRequestSet.add(id);
			long newPullRequestNumber = this.openPullRequestCount + 1;
			return new RepositoryBuilder(this)
					.withPullRequestSet(newPullRequestSet)
					.withOpenPullRequestCount(newPullRequestNumber)
					.build();
		}
		return this;
	}
	
	Repository addMergeEvent(long id, ZonedDateTime openedDate, ZonedDateTime mergedDate) {
		Set<Long> newPullRequestSet = this.pullRequestSet;
		long newPullRequestNumber = this.openPullRequestCount;
		if (pullRequestSet.contains(id)) {
			newPullRequestSet.remove(id);
			newPullRequestNumber -= 1;
		}
		long openDuration = Duration.between( openedDate , mergedDate ).toSeconds();
		long newTotalMergeTime = this.totalMergeTime + openDuration;
		long newMergeNumber = this.mergeNumber + 1;
		return new RepositoryBuilder(this)
				.withPullRequestSet(newPullRequestSet)
				.withOpenPullRequestCount(newPullRequestNumber)
				.withTotalMergeTime(newTotalMergeTime)
				.withMergeNumber(newMergeNumber)
				.build();
	}
	
	double getAvgMergeTime() {
		if (totalMergeTime == 0 || mergeNumber == 0) {
			return 0.0;
		}
		return ((double) totalMergeTime) / mergeNumber;
	}
	
	Repository addPullRequestComment() {
		long newCommentNumber = this.commentNumber + 1;
		return new RepositoryBuilder(this)
				.withCommentNumber(newCommentNumber)
				.build();
	}
	
	double getAvgCommentPerPullRequest() {
		if (openPullRequestCount == 0) {
			return 0.0;
		}
		return ((double) commentNumber) / openPullRequestCount;
	}
	
	Repository addRelease() {
		long newReleaseNumber = this.releaseNumber + 1;
		return new RepositoryBuilder(this)
				.withReleaseNumber(newReleaseNumber)
				.build();
	}
}
