package quod_ai_challenge;

import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.TimeZone;

public class Utils {
	static ZonedDateTime convertToDateTime(String str) {
		try {
			SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			dateFormater.setTimeZone(TimeZone.getTimeZone("UTC"));
			ZonedDateTime result =  ZonedDateTime.parse(str);
			return result.withZoneSameInstant(ZoneOffset.UTC);
		}
		catch (Exception e) {
			System.out.println(str);
			throw(e);
		}
	}
}
