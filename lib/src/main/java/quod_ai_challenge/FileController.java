package quod_ai_challenge;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;

public class FileController {
	static String getFileName(ZonedDateTime datetime) {
		int date = datetime.getDayOfMonth();
		int month = datetime.getMonthValue();
		int year = datetime.getYear();
		int hour = datetime.getHour();
		return String.format("%d-%02d-%02d-%d.json", year, month, date, hour);
	}
	
	static void downloadFile(String fileName) {
		System.out.println("Downloading " + fileName);
		String zipFileName = fileName + ".gz";
		String url = "https://data.gharchive.org/" + zipFileName;
		try {
			//connectionTimeout, readTimeout = 10 seconds
		    FileUtils.copyURLToFile(new URL(url), new File(zipFileName), 10000, 10000);
		    unzip(zipFileName, fileName);
		}
		catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	static void unzip(String zipFileName, String fileName) {
		System.out.println("Unziping " + fileName);
	    InputStream fin;
		try {
			Path zipFilePath = Paths.get(zipFileName);
			fin = Files.newInputStream(zipFilePath);
		    BufferedInputStream in = new BufferedInputStream(fin);
		    GZIPInputStream gzIn = new GZIPInputStream(in);
		    
		    OutputStream out = Files.newOutputStream(Paths.get(fileName));
		    
		    final byte[] buffer = new byte[1024];
		    int n = 0;
		    
		    while (-1 != (n = gzIn.read(buffer))) {
		        out.write(buffer, 0, n);
		    }
		    
		    out.close();
		    gzIn.close();
		    Files.deleteIfExists(zipFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
