package quod_ai_challenge;

import java.util.Set;

public class RepositoryBuilder {
	RepositoryBuilder(Repository repo) {
		this.id = repo.id;
		this.orgName = repo.orgName;
		this.repoName = repo.repoName;
		
		this.commitNumber = repo.commitNumber;
		
		this.developerSet = repo.developerSet;
		this.developerCount = repo.developerCount;
		
		this.issueOpenerSet = repo.issueOpenerSet;
		this.issueOpenerCount = repo.issueOpenerCount;
		this.totalIssueOpenTime = repo.totalIssueOpenTime;
		this.openedIssueNumber = repo.openedIssueNumber;
		this.closedIssueNumber = repo.closedIssueNumber;
		
		this.pullRequestSet = repo.pullRequestSet;
		this.commentNumber = repo.commentNumber;
		this.openPullRequestCount = repo.openPullRequestCount;
		this.totalMergeTime = repo.totalMergeTime;
		this.mergeNumber = repo.mergeNumber;
		
		this.releaseNumber = repo.releaseNumber;
	}
	
	private long id;
	private String repoName;
	private String orgName;
	
	private long commitNumber;
	
	private Set<String> developerSet;
	private long developerCount;
	
	private Set<String> issueOpenerSet;
	private long issueOpenerCount;
	private long totalIssueOpenTime;
	private long openedIssueNumber;
	private long closedIssueNumber;
	
	private Set<Long> pullRequestSet;
	private long commentNumber;
	private long openPullRequestCount;
	private long totalMergeTime;
	private long mergeNumber;
	
	private long releaseNumber;
	
	RepositoryBuilder withCommitNumber(long value) {
		this.commitNumber = value;
		return this;
	}
	
	RepositoryBuilder withDeveloperSet(Set<String> value) {
		this.developerSet = value;
		return this;
	}
	
	RepositoryBuilder withDeveloperCount(long value) {
		this.developerCount = value;
		return this;
	}
	
	RepositoryBuilder withIssueOpenerSet(Set<String> value) {
		this.issueOpenerSet = value;
		return this;
	}
	
	RepositoryBuilder withIssueOpenerCount(long value) {
		this.issueOpenerCount = value;
		return this;
	}
	
	RepositoryBuilder withTotalIssueOpenTime(long value) {
		this.totalIssueOpenTime = value;
		return this;
	}
	
	RepositoryBuilder withOpenedIssueNumber(long value) {
		this.openedIssueNumber = value;
		return this;
	}
	
	RepositoryBuilder withClosedIssueNumber(long value) {
		this.closedIssueNumber = value;
		return this;
	}
	
	RepositoryBuilder withPullRequestSet(Set<Long> value) {
		this.pullRequestSet = value;
		return this;
	}
	
	RepositoryBuilder withCommentNumber(long value) {
		this.commentNumber = value;
		return this;
	}
	
	RepositoryBuilder withOpenPullRequestCount(long value) {
		this.openPullRequestCount = value;
		return this;
	}
	
	RepositoryBuilder withTotalMergeTime(long value) {
		this.totalMergeTime = value;
		return this;
	}
	
	RepositoryBuilder withMergeNumber(long value) {
		this.mergeNumber = value;
		return this;
	}
	
	RepositoryBuilder withReleaseNumber(long value) {
		this.releaseNumber = value;
		return this;
	}
	
	Repository build() {
		return new Repository(
			this.id,
			this.orgName,
			this.repoName,
			
			this.commitNumber,
			
			this.developerSet,
			this.developerCount,
			
			this.issueOpenerSet,
			this.issueOpenerCount,
			this.totalIssueOpenTime,
			this.openedIssueNumber,
			this.closedIssueNumber,
			
			this.pullRequestSet,
			this.commentNumber,
			this.openPullRequestCount,
			this.totalMergeTime,
			this.mergeNumber,
			
			this.releaseNumber
		);
	}
}
